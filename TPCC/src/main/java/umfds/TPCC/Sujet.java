package umfds.TPCC;

public class Sujet {
    private String nom;
    private Groupe groupe;

    public Sujet(String nom) {
        this.nom = nom;
        this.groupe = null;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void setGroupe(Groupe grp) {
    	this.groupe = grp;
    }

	public Groupe getGroupe() {
		return this.groupe;
	}
}